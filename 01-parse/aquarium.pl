#!/usr/bin/env perl
use 5.024;
use warnings;
use autodie ':all';
use English '-no_match_vars';
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;

my $puzzle = load_puzzle(shift);

use JSON::PP;
say encode_json $puzzle;

sub load_puzzle ($filename) {
   my $fh =
     $filename eq '-'    # filename '-' means 'read from standard input'
     ? \*STDIN
     : do { open my $fh, '<', $filename; $fh };

   # just get everything
   my @items        = split m{\D+}mxs, scalar readline $fh;
   my $n            = sqrt(@items + 1) - 1;
   my @items_by_col = splice @items, 0, $n;
   my @items_by_row = splice @items, 0, $n;
   my @field        = map { [splice @items, 0, $n] } 1 .. $n;

   return {
      n            => $n,
      items_by_col => \@items_by_col,
      items_by_row => \@items_by_row,
      field        => \@field,
   };
} ## end sub load_puzzle ($filename)
