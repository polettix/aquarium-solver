#!/usr/bin/env perl
use 5.024;
use warnings;
use autodie ':all';
use English '-no_match_vars';
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;

use Term::ANSIColor;

my $puzzle = load_puzzle(shift);
solve_puzzle($puzzle);
print_puzzle($puzzle);

sub load_puzzle ($filename) {
   my $fh =
     $filename eq '-'    # filename '-' means 'read from standard input'
     ? \*STDIN
     : do { open my $fh, '<', $filename; $fh };

   # just get everything
   my @items        = split m{\D+}mxs, scalar readline $fh;
   my $n            = sqrt(@items + 1) - 1;
   my @items_by_col = splice @items, 0, $n;
   my @items_by_row = splice @items, 0, $n;
   my @field        = map { [splice @items, 0, $n] } 1 .. $n;

   return {
      n            => $n,
      items_by_col => \@items_by_col,
      items_by_row => \@items_by_row,
      field        => \@field,
   };
} ## end sub load_puzzle ($filename)

sub crossing ($A11, $A12, $A21, $A22) {
   return ' ' if ($A11 == $A12) && ($A11 == $A21) && ($A11 == $A22);
   return '-' if ($A11 == $A12) && ($A21 == $A22);
   return '|' if ($A11 == $A21) && ($A12 == $A22);
   return '+';
} ## end sub crossing

sub recolor ($string) {
   $string =~ s{(~+)}
{color('bold blue') . ('~' x length $1) . color('bold white')}egmsx;
   $string =~ s{(X+)}
{color('bold red') . ('X' x length $1) . color('bold white')}egmsx;
   return $string;
} ## end sub recolor ($string)

sub print_puzzle ($puzzle) {
   my $n          = $puzzle->{n};
   my @minus_one  = (-1) x $n;
   my @previous   = (-1, @minus_one, -1);
   my $horizontal = '#-------';
   my $vertical   = '|       ';
   my $i          = 0;
   print color('bold white');
   say '';
   my $cwidth = length $horizontal;
   my $by_column = join '', map {
      my $l     = length $_;
      my $delta = $cwidth - $l;
      $delta++ if $delta % 2;
      my $blanks = ' ' x ($delta / 2);
      substr $blanks . $_ . $blanks, 0, $cwidth, '';
   } '', $puzzle->{items_by_col}->@*;
   say $by_column;
   say '';
   for my $row ($puzzle->{field}->@*, [@minus_one]) {
      my @current = (-1, $row->@*, -1);
      my ($up_line, $mid1, $mid2) = ('') x 3;
      for my $j (1 .. $#current) {
         my $status =
           ($i < $n) && ($j <= $n) ? $puzzle->{status}[$i][$j - 1] : 0;
         my $pstatus =
              ($i > 0)
           && ($i < $n)
           && ($j <= $n) ? $puzzle->{status}[$i - 1][$j - 1] : 0;
         my $up = $horizontal;
         substr $up, 0, 1,
           crossing(@previous[$j - 1, $j], @current[$j - 1, $j]);
         $up =~ s{-}{ }gmxs if $current[$j] == $previous[$j];
         $up =~ s{\ }{~}gmxs if $status > 0 && $pstatus > 0;
         $up_line .= $up;

         my $left = $vertical;
         $left =~ s{\|}{ }gmxs if $current[$j] == $current[$j - 1];
         $left =~ s{\ }{~}gmxs if $status > 0;
         $mid1 .= $left;
         substr $left, 4, 1, 'X' if $status < 0;
         $mid2 .= $left;
      } ## end for my $j (1 .. $#current)
      $_ = recolor($_) for ($up_line, $mid1, $mid2);
      s{\s+\z}{}mxs for ($up_line, $mid1, $mid2);

      (my $left_blanks = $vertical) =~ s{\S}{ }gmxs;
      say $left_blanks, $up_line;
      $mid1 = $left_blanks . $mid1;
      my $n = length($left_blanks) - 2;
      $left_blanks = sprintf "%${n}s  ", $puzzle->{items_by_row}[$i]
        if $mid2 =~ m{\S}mxs;
      $mid2 = $left_blanks . $mid2;
      do { say for $mid1, $mid2, $mid1 } if $mid2 =~ m{\S}mxs;
      @previous = @current;
      ++$i;
   } ## end for my $row ($puzzle->{...})
   say '';
   print color('reset');
} ## end sub print_puzzle ($puzzle)

sub solve_puzzle ($puzzle) {    # put a fixed solution
   my $sol = '-1 -1  1  1  1  1
               1  1  1  1  1 -1
               1  1  1  1  1 -1
               1  1  1 -1 -1 -1
              -1  1 -1 -1 -1 -1
               1  1  1 -1 -1 -1';
   $sol =~ s{^\s+}{}gmxs;
   $puzzle->{status} = [map { [split m{\s+}mxs] } split m{\n+}, $sol];
   return $puzzle;
} ## end sub solve_puzzle ($puzzle)
