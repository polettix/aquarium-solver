#!/usr/bin/env perl
use 5.024;
use warnings;
use autodie ':all';
use English '-no_match_vars';
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;

use Term::ANSIColor;
use Try::Catch;
use Storable 'dclone';

$|++;
my $puzzle = load_puzzle(shift);
solve_puzzle($puzzle);
print_puzzle($puzzle);

sub load_puzzle ($filename) {
   my $fh =
     $filename eq '-'    # filename '-' means 'read from standard input'
     ? \*STDIN
     : do { open my $fh, '<', $filename; $fh };

   # just get everything
   my @items        = split m{\D+}mxs, scalar readline $fh;
   my $n            = sqrt(@items + 1) - 1;
   my @items_by_col = splice @items, 0, $n;
   my @items_by_row = splice @items, 0, $n;
   my @field        = map { [splice @items, 0, $n] } 1 .. $n;

   return {
      n            => $n,
      items_by_col => \@items_by_col,
      items_by_row => \@items_by_row,
      field        => \@field,
   };
} ## end sub load_puzzle ($filename)

sub crossing ($A11, $A12, $A21, $A22) {
   return ' ' if ($A11 == $A12) && ($A11 == $A21) && ($A11 == $A22);
   return '-' if ($A11 == $A12) && ($A21 == $A22);
   return '|' if ($A11 == $A21) && ($A12 == $A22);
   return '+';
} ## end sub crossing

sub recolor ($string) {
   $string =~ s{(~+)}
{color('bold blue') . ('~' x length $1) . color('bold white')}egmsx;
   $string =~ s{(X+)}
{color('bold red') . ('X' x length $1) . color('bold white')}egmsx;
   return $string;
} ## end sub recolor ($string)

sub print_puzzle ($puzzle) {
   my $n          = $puzzle->{n};
   my @minus_one  = (-1) x $n;
   my @previous   = (-1, @minus_one, -1);
   my $horizontal = '#-------';
   my $vertical   = '|       ';
   my $i          = 0;
   print color('bold white');
   say '';
   my $cwidth = length $horizontal;
   my $by_column = join '', map {
      my $l     = length $_;
      my $delta = $cwidth - $l;
      $delta++ if $delta % 2;
      my $blanks = ' ' x ($delta / 2);
      substr $blanks . $_ . $blanks, 0, $cwidth, '';
   } '', $puzzle->{items_by_col}->@*;
   say $by_column;
   say '';
   for my $row ($puzzle->{field}->@*, [@minus_one]) {
      my @current = (-1, $row->@*, -1);
      my ($up_line, $mid1, $mid2) = ('') x 3;
      for my $j (1 .. $#current) {
         my $status =
           ($i < $n) && ($j <= $n) ? $puzzle->{status}[$i][$j - 1] : 0;
         my $pstatus =
              ($i > 0)
           && ($i < $n)
           && ($j <= $n) ? $puzzle->{status}[$i - 1][$j - 1] : 0;
         my $up = $horizontal;
         substr $up, 0, 1,
           crossing(@previous[$j - 1, $j], @current[$j - 1, $j]);
         $up =~ s{-}{ }gmxs if $current[$j] == $previous[$j];
         $up =~ s{\ }{~}gmxs if $status > 0 && $pstatus > 0;
         $up_line .= $up;

         my $left = $vertical;
         $left =~ s{\|}{ }gmxs if $current[$j] == $current[$j - 1];
         $left =~ s{\ }{~}gmxs if $status > 0;
         $mid1 .= $left;
         substr $left, 4, 1, 'X' if $status < 0;
         $mid2 .= $left;
      } ## end for my $j (1 .. $#current)
      $_ = recolor($_) for ($up_line, $mid1, $mid2);
      s{\s+\z}{}mxs for ($up_line, $mid1, $mid2);

      (my $left_blanks = $vertical) =~ s{\S}{ }gmxs;
      say $left_blanks, $up_line;
      $mid1 = $left_blanks . $mid1;
      my $n = length($left_blanks) - 2;
      $left_blanks = sprintf "%${n}s  ", $puzzle->{items_by_row}[$i]
        if $mid2 =~ m{\S}mxs;
      $mid2 = $left_blanks . $mid2;
      do { say for $mid1, $mid2, $mid1 } if $mid2 =~ m{\S}mxs;
      @previous = @current;
      ++$i;
   } ## end for my $row ($puzzle->{...})
   say '';
   print color('reset');
} ## end sub print_puzzle ($puzzle)

sub adjust_empty_level ($puzzle) {
   my ($n, $field, $status) = $puzzle->@{qw< n field status >};
   my $n_changes = 0;
   for my $i (reverse 0 .. $n - 1) {    # iterate rows from bottom to top
      my %expected;

      # first sweep: adjust vertical emptying, set expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         # vertical condition from before-last row on...
         if (($i < $n - 1) && ($id == $field->[$i + 1][$j])) {
            if ($st > $status->[$i + 1][$j]) { # possible mismatch?
               if ($st == 0) { # current cell is *unknown*, relax!
                  $st = $status->[$i][$j] = -1;  # mark empty
                  $n_changes++;
               }
               elsif ($status->[$i - 1][$j] != 0) {
                  die "wrong vertical leveling for aquarium $id\n";
               }
            }
         }

         $expected{$id} ||= $st; # change only if unknown
      }
   
      # second sweep: adjust horizontal emptying based on expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         if ($st == 0) {
            if ($expected{$id}) {
               $st = $status->[$i][$j] = $expected{$id};
               $n_changes++;
            }
         }
         elsif ($st != $expected{$id}) {
            die "wrong horizontal leveling for aquarium $id\n"
         }
      } ## end for my $j (0 .. $n - 1)
   } ## end for my $i (0 .. $n - 1)
   return $n_changes;
} ## end sub assert_water_level ($puzzle)

sub adjust_water_level ($puzzle) {
   my ($n, $field, $status) = $puzzle->@{qw< n field status >};
   my $n_changes = 0;
   for my $i (0 .. $n - 1) {    # iterate rows from top to bottom
      my %expected;

      # first sweep: adjust vertical flooding, set expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         # vertical condition from second row on...
         if (($i > 0) && ($id == $field->[$i - 1][$j])) {
            if ($st < $status->[$i - 1][$j]) { # possible mismatch?
               if ($st == 0) { # current cell is *unknown*, relax!
                  $st = $status->[$i][$j] = 1;  # fill with water
                  $n_changes++;
               }
               elsif ($status->[$i - 1][$j] != 0) {
                  die "wrong vertical leveling for aquarium $id\n";
               }
            }
         }

         $expected{$id} ||= $st; # change only if unknown
      }
   
      # second sweep: adjust horizontal flooding based on expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         if ($st == 0) {
            if ($expected{$id}) {
               $st = $status->[$i][$j] = $expected{$id};
               $n_changes++;
            }
         }
         elsif ($st != $expected{$id}) {
            die "wrong horizontal leveling for aquarium $id\n"
         }
      } ## end for my $j (0 .. $n - 1)
   } ## end for my $i (0 .. $n - 1)
   return $n_changes;
} ## end sub assert_water_level ($puzzle)

sub adjust_by_row ($puzzle) {
   my ($n, $items_by_row, $field, $status)
      = $puzzle->@{qw< n items_by_row field status >};
   my $acted = 0;
   for my $i (0 .. $n - 1) {
      my $needed = $items_by_row->[$i];
      my $available = $n;
      my %available_by_id;
      for my $j (0 .. $n - 1) {
         $needed-- if $status->[$i][$j] > 0;
         $available-- if $status->[$i][$j];
         my $id = $field->[$i][$j];
         push $available_by_id{$id}->@*, $j unless $status->[$i][$j];
      }
      die 'unfeasible' if $needed < 0;
      die 'unfeasible' if $needed > $available;
      for my $id (keys %available_by_id) {
         my $av = $available_by_id{$id};
         if ($av->@* > $needed) { # can't serve this here
            $status->[$i][$_] = -1 for $av->@*;
            $acted++;
         }
         elsif ($available - $av->@* < $needed) { # need this
            $status->[$i][$_] = 1 for $av->@*;
            $acted++;
         }
      }
   }
   return $acted;
}

sub adjust_by_col ($puzzle) {
   my ($n, $items_by_col, $field, $status)
      = $puzzle->@{qw< n items_by_col field status >};
   my $acted = 0;
   for my $j (0 .. $n - 1) {
      my $needed = $items_by_col->[$j];
      my $available = $n;
      my %available_by_id;
      for my $i (0 .. $n - 1) {
         $needed-- if $status->[$i][$j] > 0;
         $available-- if $status->[$i][$j];
         my $id = $field->[$i][$j];
         push $available_by_id{$id}->@*, $i unless $status->[$i][$j];
      }
      die 'unfeasible' if $needed < 0;
      die 'unfeasible' if $needed > $available;
      for my $id (keys %available_by_id) {
         my $av = $available_by_id{$id};
         if ($available - $av->@* < $needed) { # need this
            my $take = $needed - ($available - $av->@*);
            $status->[pop $av->@*][$j] = 1 for 1 .. $take;
            $available -= $take;
            $needed -= $take;
            $acted++;
         }
         elsif ($av->@* > $needed) { # remove excess
            $status->[shift $av->@*][$j] = -1 while $av->@* > $needed;
            $acted++;
         }
         # there should be more to this
      }
   }
   return $acted;
}

sub apply_constraints ($puzzle) {
   my $changes = -1;
   while ($changes != 0) {
      $changes = 0;
      $changes += adjust_water_level($puzzle);
      $changes += adjust_empty_level($puzzle);
      $changes += adjust_by_col($puzzle);
      $changes += adjust_by_row($puzzle);
   }
}

sub moves_iterator ($puzzle) {
   my ($n, $field, $status) = $puzzle->@{qw< n field status >};

   my ($best_row, $best_id, $best_count);
   for my $row (0 .. $n - 1) {
      my %count_for;
      for my $j (0 .. $n - 1) {
         next if $status->[$row][$j];
         $count_for{$field->[$row][$j]}++;
      }
      for my $id (sort {$a <=> $b} keys %count_for) {
         my $count = $count_for{$id};
         ($best_row, $best_id, $best_count) = ($row, $id, $count)
            if (! defined $best_row) || ($best_count < $count);
      }
   }

   my $alt_status = dclone($status);
   for my $j (0 .. $n - 1) {
      next unless $field->[$best_row][$j] == $best_id;
      $status->[$best_row][$j] = 1;
      $alt_status->[$best_row][$j] = -1;
   }

   my @retval = ($status, $alt_status);
   return sub { return shift @retval };
}

sub is_complete ($puzzle) {
   my ($n, $items_by_row, $status) = $puzzle->@{qw< n items_by_row status >};
   my $missing = 0;
   for my $i (0 .. $n - 1) {
      $missing += $items_by_row->[$i];
      for my $j (0 .. $n - 1) {
         $missing-- if $status->[$i][$j] > 0;
      }
   }
   return $missing == 0;
}

sub solve_puzzle ($puzzle) {
   my $n = $puzzle->{n};
   $puzzle->{status} = [ map { [(0) x $n] } 1 .. $n ];
   my @stack;
   my $done;
   while (! $done) {
      try {
         apply_constraints($puzzle);

         # if there are still unknown items, let's take a guess
         $done = is_complete($puzzle);
         if (! $done) {
            my $guesser = moves_iterator($puzzle);
            my $status = $guesser->(scalar @stack) # do first guess!
               or die "no more guesses here\n";

            # save guesser for backtracking
            push @stack, $guesser;

            # of course this is the new status
            $puzzle->{status} = $status;
         }
      }
      catch {
         (my $e = $_) =~ s{\s+\z}{}gmxs;
         while (@stack) { # backtrack until there's a new guess
            if (my $status = $stack[-1]->(scalar @stack - 1)) {
               $puzzle->{status} = $status;
               last;
            }
            pop @stack;
         }
         die "unfeasible <$e>\n" unless @stack;
      };
   }
   return $puzzle;
}
